## Sistema de Cartoes Fiap

```
A FIAP resolveu criar seu próprio cartão de crédito para ser 
utilizado pelos alunos e para isso necessita de um sistema 
para gerenciamento e integração com outras empresas.
```

## Pre Requisitos 

```
Java8
Maven3 
```

## Download

```
https://bitbucket.org/thgveiga/cartao/src/master/
git clone https://thgveiga@bitbucket.org/thgveiga/cartao.git
git clone git@bitbucket.org:thgveiga/cartao.git
```

## Instalação

```bash
mvn clean install 
java -jar {user_home}/.m2/repository/org/projectlombok/lombok/1.18.12/lombok-1.18.12.jar
mvn clean spring-boot:run
```

## Swagger

```
http://localhost:8080/swagger-ui.html

```

## Database
```
url : http://localhost:8080/h2-console
jdbc- url : jdbc:h2:mem:testdb
user : sa
password : ''
```

## Massa de dados para popular o database 
```
executar o post na url http://localhost:8080/api/v1/alunos/list
passando o arquivo json cartao\src\main\resources\data\popula.json como parametro no corpo da requisição
```
## Integração com autorizada 
```
pode ser feita atraves da api http://localhost:8080//api/v1/compras
```
## Executar testes 
```
mvn test
```

## Obserrvações
```
o endpoint http://localhost:8080//api/v1/alunos/extrato/{id}
retorna um arquivo pdf , recomenda-se efetuar o teste em um browser ao inves da interface do swagger
pois a mesma apresentou problemas para efetuar o download do pdf
```

## Contribuição
```
Pull requests são bem vindos.
```

## License
```
OpenSource
```

## Ferramentas utilizadas e justificativas 
```
java - pre requisito 
spring - pre requisito
swagger - pre requisito
rest - facilidade e padronização de uso e integracao
itext - api madura para gerar pdf 
lombok - facilidade e agilidade de desenvolvimento
springboot - container embarcado e facilidade de build e start up da aplicação
banco de dados h2 - banco de dados em memoria de facil utilização para testar a aplicação 
```