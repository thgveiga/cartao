var alunos = [];

function findAluno (alunoId) {
  return alunos[findAlunoKey(alunoId)];
}

function findAlunoKey (alunoId) {
  for (var key = 0; key < alunos.length; key++) {
    if (alunos[key].id == alunoId) {
      return key;
    }
  }
}

var alunoService = {
  findAll(fn) {
    axios
      .get('/api/v1/alunos')
      .then(response => fn(response))
      .catch(error => console.log(error))
  },

  findById(id, fn) {
    axios
      .get('/api/v1/alunos/id/' + id)
      .then(response => fn(response))
      .catch(error => console.log(error))
  },

  create(aluno, fn) {
    axios
      .post('/api/v1/alunos', aluno)
      .then(response => fn(response))
      .catch(error => console.log(error))
  },

  update(id, aluno, fn) {
    axios
      .put('/api/v1/alunos/' + id, aluno)
      .then(response => fn(response))
      .catch(error => console.log(error))
  },

  deleteAluno(id, fn) {
    axios
      .delete('/api/v1/alunos/' + id)
      .then(response => fn(response))
      .catch(error => console.log(error))
  }
}

var List = Vue.extend({
  template: '#aluno-list',
  data: function () {
    return {alunos: [], searchKey: ''};
  },
  computed: {
    filteredAlunos() {
      return this.alunos.filter((aluno) => {
      	return aluno.name.indexOf(this.searchKey) > -1
      	  || aluno.description.indexOf(this.searchKey) > -1
      	  || aluno.price.toString().indexOf(this.searchKey) > -1
      })
    }
  },
  mounted() {
    alunoService.findAll(r => {this.alunos = r.data; alunos = r.data})
  }
});

var Aluno = Vue.extend({
  template: '#aluno',
  data: function () {
    return {aluno: findAluno(this.$route.params.aluno_id)};
  }
});

var AlunoEdit = Vue.extend({
  template: '#aluno-edit',
  data: function () {
    return {aluno: findAluno(this.$route.params.aluno_id)};
  },
  methods: {
    updateAluno: function () {
      alunoService.update(this.aluno.id, this.aluno, r => router.push('/'))
    }
  }
});

var AlunoDelete = Vue.extend({
  template: '#aluno-delete',
  data: function () {
    return {aluno: findAluno(this.$route.params.aluno_id)};
  },
  methods: {
    deleteAluno: function () {
      alunoService.deleteAluno(this.aluno.id, r => router.push('/'))
    }
  }
});

var AddAluno = Vue.extend({
  template: '#add-aluno',
  data() {
    return {
      aluno: {name: '', description: '', price: 0}
    }
  },
  methods: {
    createAluno() {
      alunoService.create(this.aluno, r => router.push('/'))
    }
  }
});

var router = new VueRouter({
	routes: [
		{path: '/', component: List},
		{path: '/aluno/id/:aluno_id', component: Aluno, name: 'aluno'},
		{path: '/add-aluno', component: AddAluno},
		{path: '/aluno/:aluno_id/edit', component: AlunoEdit, name: 'aluno-edit'},
		{path: '/aluno/:aluno_id/delete', component: AlunoDelete, name: 'aluno-delete'}
	]
});

new Vue({
  router
}).$mount('#app')
