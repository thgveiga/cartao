package com.fiap.cartao.domain.compra;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fiap.cartao.domain.cartaocredito.CartaoCredito;
import com.fiap.cartao.domain.cartaocredito.CartaoCreditoService;

@Service

public class CompraService {

	@Autowired
	private CompraRepository compraRespository;

	@Autowired
	private CartaoCreditoService cartaoCreditoService;

	public List<Compra> findAll() {
		return compraRespository.findAll();
	}

	public Optional<Compra> findById(Long id) {
		return compraRespository.findById(id);
	}

	public Optional<Compra> findByDescricao(String descricao) {
		return compraRespository.findByDescricao(descricao);
	}

	public Optional<List<Compra>> findByNumeroCartao(Long numeroCartao) {
		Optional<CartaoCredito> optional = cartaoCreditoService.findByNumero(numeroCartao);

		if (!optional.isPresent())
			throw new IllegalArgumentException(
					String.format("O Cartao de credito de numero: %d não existe ", numeroCartao));

		CartaoCredito cartaoCredito = optional.get();
		return Optional.ofNullable(cartaoCredito.getCompras());
	}

	public Optional<List<Compra>> findByNomeTitular(String nomeTitular) {
		Optional<CartaoCredito> optional = cartaoCreditoService.findByTitular(nomeTitular);

		if (!optional.isPresent())
			throw new IllegalArgumentException(
					String.format("O Cartao de credito de titularidade: %s não existe ", nomeTitular));

		CartaoCredito cartaoCredito = optional.get();
		return Optional.ofNullable(cartaoCredito.getCompras());
	}

	public Compra save(Compra compra) {
		Optional<CartaoCredito> optional = cartaoCreditoService.findByNumero(compra.getNumeroCartao());
		
		if(!optional.isPresent())
			throw new IllegalArgumentException(
					String.format("O Cartao de credito de numero: %d não existe ", compra.getNumeroCartao()));
		
		CartaoCredito cartaoCredito = optional.get();
		
		Compra compraSalva = compraRespository.save(compra);

		List<Compra> compras = cartaoCredito.getCompras();
		compras.add(compraSalva);

		cartaoCredito.setCompras(compras);
		
		cartaoCreditoService.update(cartaoCredito.getId(), cartaoCredito);
		
		return compraSalva;
	}

	public void delete(List<Compra> compras) {
		compraRespository.deleteInBatch(compras);
	}
	
	
	public Compra update(Long id, @Valid Compra compraNovo) {

		Optional<Compra> optional = compraRespository.findById(id);

		if (!optional.isPresent())
			throw new IllegalArgumentException(
					String.format("Compra com id %d não encontrado para efetuar atualizacao ", id));

		Compra compraAtual = optional.get();
		
		if( ! compraAtual.getNomeTitular().equals(compraNovo.getNomeTitular()) || compraAtual.getNumeroCartao().equals(compraNovo.getNumeroCartao()))
			throw new IllegalStateException("nao é possivel alterar o titular ou numero de cartao de uma compra");
		
		compraAtual.setDescricao(compraNovo.getDescricao());
		compraAtual.setNumeroDeParcelas(compraNovo.getNumeroDeParcelas());
		compraAtual.setValor(compraNovo.getValor());
		compraAtual.setUpdatedAt(LocalDateTime.now());

		return compraRespository.save(compraAtual);

	}

	public void deleteById(Long id) {

		Optional<Compra> optional = compraRespository.findById(id);

		if (!optional.isPresent())
			throw new IllegalArgumentException(
					String.format("Compra com id %d não encontrado para efetuar atualizacao ", id));

		Compra compra = optional.get();
		Optional<CartaoCredito> optionalCartao = cartaoCreditoService.findByNumero(compra.getNumeroCartao());
		
		if (!optionalCartao.isPresent())
			throw new IllegalArgumentException(
					String.format("Cartao de credito vinculado a compra nao existe numero do cartao: %s titutal: %s ", compra.getNumeroCartao(), compra.getNomeTitular()));

		CartaoCredito cartaoCredito = optionalCartao.get();
		List<Compra> compras = cartaoCredito.getCompras();
		
		if(compras != null && !compras.isEmpty()) {
			compras.remove(compra);
		    cartaoCredito.setCompras(compras);
		    cartaoCreditoService.update(cartaoCredito.getId(), cartaoCredito);
		}
		
		compraRespository.deleteById(id);
	}
}
