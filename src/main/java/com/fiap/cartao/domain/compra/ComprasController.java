package com.fiap.cartao.domain.compra;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ComprasController {

	@GetMapping("/compras")
    public String list(){
        return "compras";
    }
	
}
