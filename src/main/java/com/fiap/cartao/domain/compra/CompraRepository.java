package com.fiap.cartao.domain.compra;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CompraRepository extends JpaRepository<Compra, Long> {

	Optional<Compra> findByDescricao(String descricao);


	
}
