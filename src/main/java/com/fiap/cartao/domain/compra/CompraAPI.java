package com.fiap.cartao.domain.compra;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fiap.cartao.domain.cartaocredito.CartaoCredito;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1/compras")
@Slf4j

public class CompraAPI {
	
	@Autowired
	private CompraService compraService;

    @GetMapping
	public ResponseEntity<List<Compra>> findAll() {
		return ResponseEntity.ok(compraService.findAll()); 
	}
    
    @GetMapping("/id/{id}")
	public ResponseEntity<Compra> findById(@PathVariable Long id) {
    	
    	Optional<Compra> compraOptional = compraService.findById(id);
		if (!compraOptional.isPresent()) {
			log.info("Compra com Id " + id + " nao encontrado");
		}

		return ResponseEntity.ok(compraOptional.get());
		
	}
    
    @GetMapping("/descricao/{descricao}")
	public ResponseEntity<Compra> findByDescricao(@PathVariable String descricao) {
    	
    	Optional<Compra> compraOptional = compraService.findByDescricao(descricao);
		if (!compraOptional.isPresent()) {
			log.info("Compra com descricao " + descricao + " nao encontrado");
		}

		return ResponseEntity.ok(compraOptional.get());
		
	}
    
    @GetMapping("/numeroCartao/{numeroCartao}")
	public ResponseEntity<List<Compra>> findByNumeroCartao(@PathVariable Long numeroCartao) {
    	
    	Optional<List<Compra>> compraOptional = compraService.findByNumeroCartao(numeroCartao);
		if (!compraOptional.isPresent()) {
			log.info("Compra com Numero de Cartao " + numeroCartao + " nao encontrado");
		}

		return ResponseEntity.ok(compraOptional.get());
		
	}
    
    @GetMapping("/nomeTitular/{nomeTitular}")
	public ResponseEntity<List<Compra>> findByNomeTitular(@PathVariable String nomeTitular) {
    	
    	Optional<List<Compra>> compraOptional = compraService.findByNomeTitular(nomeTitular);
		if (!compraOptional.isPresent()) {
			log.info("Compra com Nome do Titular " + nomeTitular + " nao encontrado");
		}

		return ResponseEntity.ok(compraOptional.get());
		
	}
    
	@PostMapping
	public ResponseEntity<Compra> create(@Valid @RequestBody Compra compra) {
		try {
			return ResponseEntity.ok(compraService.save(compra));	
		} catch (Exception e) {
			log.error(String.format("nao foi possivel salvar a compra com valores: %s" , compra));
			return ResponseEntity.badRequest().build();
		}
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Compra> update(@PathVariable Long id, @Valid @RequestBody Compra compra) {
		
		try {
			return ResponseEntity.ok(compraService.update(id, compra));	
		} catch (Exception e) {
			log.error(String.format("nao foi possivel atualizar a compra %s com valores: %s" , id, compra));
			return ResponseEntity.badRequest().build();
		}
        
	}

	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable Long id) {
		if (!compraService.findById(id).isPresent()) {
			log.error("Compra com Id " + id + " nao encontrado");
			return ResponseEntity.badRequest().build();
		}

		compraService.deleteById(id);

		return ResponseEntity.ok().build();
	}

}
