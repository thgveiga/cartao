package com.fiap.cartao.domain.aluno;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AlunoRepository extends JpaRepository<Aluno, Long> {

	Optional<Aluno> findByRa(String ra);

	Optional<Aluno> findByNome(String nome);

	void save(List<Aluno> alunos);

	@Query("from Aluno a where a.turma = :turma")
	List<Aluno> findByTurma(String turma);
}
