package com.fiap.cartao.domain.aluno;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.validation.Valid;

import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fiap.cartao.domain.cartaocredito.CartaoCredito;
import com.fiap.cartao.domain.compra.Compra;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j

public class AlunoService {
	@Autowired
 	private AlunoRepository alunoRespository;

	private Path rootLocation;

	public AlunoService() {
		rootLocation = Paths.get(".");
	}

	public List<Aluno> findAll() {
		return alunoRespository.findAll();
	}

	public Optional<Aluno> findById(Long id) {
		return alunoRespository.findById(id);
	}

	public Optional<Aluno> findByNome(String nome) {
		return alunoRespository.findByNome(nome);
	}
	
	public Optional<Aluno> findByRa(String ra) {
		return alunoRespository.findByRa(ra);
	}
	
	public List<Aluno> findByTurma(String turma) {
		return alunoRespository.findByTurma(turma);
	}
	
	public Aluno save(@Valid Aluno aluno) {
		Optional<Aluno> optional = alunoRespository.findByRa(aluno.getRa());
		
		if(optional.isPresent())
			throw new IllegalArgumentException(String.format("Aluno já cadastrado - RA: ", aluno.getRa()));
		
		return alunoRespository.save(aluno);
	}	

	public List<Aluno> save(@Valid List<Aluno> alunos) {
		return alunoRespository.saveAll(alunos);
	}
	
	public Aluno deleteById(Long id) {
		Optional<Aluno> optional = alunoRespository.findById(id);
		
		if(!optional.isPresent())
			throw new IllegalArgumentException(String.format("Aluno com id %d não encntrado para ser removido ", id));
		
		alunoRespository.deleteById(id);
		return optional.get();
	}
	
	public Aluno update(Long id, @Valid Aluno aluno) {
		
		Optional<Aluno> optional = alunoRespository.findById(id);
		
		if(!optional.isPresent())
			throw new IllegalArgumentException(String.format("Aluno com id %d não encntrado para efetuar atualizacao ", id));
		
		Optional<Aluno> optionalRA = alunoRespository.findByRa(aluno.getRa());
		
		if(optionalRA.isPresent())
			throw new IllegalArgumentException(String.format("RA já utilizado - RA: ", aluno.getRa()));
		
		Aluno temp = optional.get();
		temp.setNome(aluno.getNome());
		temp.setRa(aluno.getRa());
		temp.setTurma(aluno.getTurma());
		temp.setCartaoCredito(aluno.getCartaoCredito());
		temp.setUpdatedAt(LocalDateTime.now());
		return alunoRespository.save(temp);
		
	}

	public void store(MultipartFile file) throws FileUploadException, IOException {

		if (file.isEmpty()) {
			throw new FileUploadException("o arquivo esta vazio");
		}

		String filename = StringUtils.cleanPath(file.getOriginalFilename());

		saveFile(filename, file);
		importContentFile(filename, file);

	}

	private void importContentFile(String filename, MultipartFile file) {

		Runnable task = () -> {

			List<Aluno> alunos = new ArrayList<Aluno>();

			try (Stream<String> stream = Files.lines(Paths.get("./" + filename))) {

				stream.forEach(
						(line) -> {
								     Aluno aluno = linhaToAluno(line);
								     if (aluno != null)
									    alunos.add(aluno);
				                  }
			);

			} catch (IOException e) {
				log.error("erro ao importar o arquivo para o banco de dados " + e.getMessage());
			} 
			
			alunoRespository.saveAll(alunos);
		};

		Thread thread = new Thread(task);
		thread.start();

	}

	private Aluno linhaToAluno(String line) {
		
		if(!(line != null && !line.isEmpty() && !line.contains("--------") && line.length() > 14))
			return null;
		
		Pattern pattern = Pattern.compile("[0-9]+[\\s][0-9]+[-][0-9]+");
        Matcher matcher = pattern.matcher(line);
        
        if(matcher.find()) {
        	String nome = line.replaceAll(matcher.group(), "").trim();
        	String ra = matcher.group().split(" ")[0].trim();
        	String turma = matcher.group().split(" ")[1].trim();
        	   if(findByRa(ra).isPresent()) {
        		  log.info("RA já cadastrado: " + ra);
   			      return null;
        	   }else
        		   return new Aluno(nome, ra,turma);	   
        }
        
		return null;
		
	}

	private void saveFile(String filename, MultipartFile file) throws IOException {

		try (InputStream inputStream = file.getInputStream()) {
			Files.copy(inputStream, this.rootLocation.resolve(filename), StandardCopyOption.REPLACE_EXISTING);
		}

	}

	public ByteArrayInputStream extrato(Long id) throws DocumentException {
		Optional<Aluno> alunoOpt = alunoRespository.findById(id);
		if (alunoOpt.isPresent()) {
			Aluno aluno = alunoOpt.get();
			ByteArrayInputStream byteArrayInputStream = geraPdf(aluno.getCartaoCredito());
            return byteArrayInputStream; 
		}else {
			throw new IllegalArgumentException(String.format("Aluno com id %d não encntrado para gerar extrato ", id));
		}

	}

	private ByteArrayInputStream geraPdf(CartaoCredito cartaoCredito) throws DocumentException {

		String titular = cartaoCredito.getTitular();
		Long numeroCartaoCredito = cartaoCredito.getNumero();
		
		Document document = new Document();
		document.addTitle(String.format("Titular: %s Cartão: %d", titular, numeroCartaoCredito));
				
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(60);
		table.setWidths(new int[] { 1, 3, 3 });

		Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

		PdfPCell hcell;
		hcell = new PdfPCell(new Phrase("Id", headFont));
		hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(hcell);

		hcell = new PdfPCell(new Phrase("Descrição", headFont));
		hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(hcell);

		hcell = new PdfPCell(new Phrase("Valor", headFont));
		hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(hcell);

		hcell = new PdfPCell(new Phrase("Número de Parcelas", headFont));
		hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(hcell);
		
		hcell = new PdfPCell(new Phrase("Data", headFont));
		hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(hcell);

		for (Compra compra : cartaoCredito.getCompras()) {

			PdfPCell cell;

			cell = new PdfPCell(new Phrase(compra.getId().toString()));
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(compra.getDescricao()));
			cell.setPaddingLeft(5);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(NumberFormat.getCurrencyInstance().format(compra.getValor())));
			cell.setPaddingLeft(5);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(cell);
			
			cell = new PdfPCell(new Phrase(compra.getNumeroDeParcelas()));
			cell.setPaddingLeft(5);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(cell);
			
			cell = new PdfPCell(new Phrase(compra.getUpdatedAt().format(DateTimeFormatter.ISO_DATE_TIME)));
			cell.setPaddingLeft(5);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setPaddingRight(5);
			table.addCell(cell);
		}

		PdfWriter.getInstance(document, out);
		document.open();
		document.add(table);

		document.close();

		return new ByteArrayInputStream(out.toByteArray());

	}

}
