package com.fiap.cartao.domain.aluno;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AlunoController {

	@GetMapping("/alunos")
    public String list(){
        return "alunos";
    }
	
}
