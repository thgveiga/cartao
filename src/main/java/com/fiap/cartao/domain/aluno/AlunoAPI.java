package com.fiap.cartao.domain.aluno;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.itextpdf.text.DocumentException;

import lombok.var;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1/alunos")
@Slf4j
public class AlunoAPI {
	
	@Autowired
	private AlunoService alunoService;

	@GetMapping
	public ResponseEntity<List<Aluno>> findAll() {
		return ResponseEntity.ok(alunoService.findAll());
	}

	@PostMapping
	public ResponseEntity<Aluno> create(@Valid @RequestBody Aluno aluno) {
		return ResponseEntity.ok(alunoService.save(aluno));
	}

	@PostMapping("/list")
	public ResponseEntity<List<Aluno>> createList(@Valid @RequestBody List<Aluno> alunos) {
		return ResponseEntity.ok(alunoService.save(alunos));
	}
	
	@GetMapping("/id/{id}")
	public ResponseEntity<Aluno> findById(@PathVariable Long id) {
		Optional<Aluno> aluno = alunoService.findById(id);
		if (!aluno.isPresent()) {
			log.info("Aluno com Id " + id + " nao encontrado");
		}

		return ResponseEntity.ok(aluno.get());
	}

	@GetMapping("/ra/{ra}")
	public ResponseEntity<Aluno> findByRa(@PathVariable String ra) {
		Optional<Aluno> aluno = alunoService.findByRa(ra);
		if (!aluno.isPresent()) {
			log.info("Aluno com RA " + ra + " nao encontrado");
		}

		return ResponseEntity.ok(aluno.get());
	}
	
	@GetMapping("/nome/{nome}")
	public ResponseEntity<Aluno> findByNome(@PathVariable String nome) {
		Optional<Aluno> aluno = alunoService.findByNome(nome);
		if (!aluno.isPresent()) {
			log.info("Aluno com nome " + nome + " nao encontrado");
		}

		return ResponseEntity.ok(aluno.get());
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Aluno> update(@PathVariable Long id, @Valid @RequestBody Aluno aluno) {
		try {
			return ResponseEntity.ok(alunoService.update(id, aluno));	
		} catch (Exception e) {
			log.error(String.format("nao foi possivel atualizar o aluno %d com valores: %s", id, aluno));
	        return ResponseEntity.badRequest().build();
		}
		
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Aluno> delete(@PathVariable Long id) {
		
		try {
			return ResponseEntity.ok(alunoService.deleteById(id));	
		} catch (Exception e) {
			log.error(String.format("nao foi possivel atualizar o aluno %d ", id));
	        return ResponseEntity.badRequest().build();
		}
	}
	
	
	@PostMapping("/import")
	public ResponseEntity<Object> handleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {

		try {
			alunoService.store(file);
			
			String msg = "Arquivo importado com sucesso " + file.getOriginalFilename() + "!";
			redirectAttributes.addFlashAttribute("message", msg);
	        log.info(msg);
		    return ResponseEntity.ok().build();

		} catch (FileUploadException | IOException e) {
			log.error("erro ao importar arquivo " + e.getMessage());
			
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		
	}
	
    @RequestMapping(value = "/extrato/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)

	public ResponseEntity<InputStreamResource>  extrato(@PathVariable Long id) {
    	var headers = new HttpHeaders();
		try {
			 ByteArrayInputStream bis = alunoService.extrato(id);
		     headers.add("Content-Disposition", "inline; filename=citiesreport.pdf");
		        return ResponseEntity
		                .ok()
		                .headers(headers)
		                .contentType(MediaType.APPLICATION_PDF)
		                .body(new InputStreamResource(bis));
		} catch (DocumentException e) {
			log.error("falha ao gerar pdf " + e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
	}
}
