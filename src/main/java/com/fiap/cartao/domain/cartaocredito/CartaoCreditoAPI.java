package com.fiap.cartao.domain.cartaocredito;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1/cartoes")
@Slf4j

public class CartaoCreditoAPI {
	
	@Autowired
	private CartaoCreditoService cartaoCreditoService;

    @GetMapping
	public ResponseEntity<List<CartaoCredito>> findAll() {
		return ResponseEntity.ok(cartaoCreditoService.findAll());
	}

    @GetMapping("/id/{id}")
	public ResponseEntity<CartaoCredito> findById(@PathVariable Long id) {
    	
    	Optional<CartaoCredito> carOptional = cartaoCreditoService.findById(id);
		if (!carOptional.isPresent()) {
			log.info("Cartao de credito com Id " + id + " nao encontrado");
		}

		return ResponseEntity.ok(carOptional.get());
		
	}
    
    @GetMapping("/numero/{numero}")
	public ResponseEntity<CartaoCredito> findByNumero(@PathVariable Long numero) {
    	
    	Optional<CartaoCredito> carOptional = cartaoCreditoService.findByNumero(numero);
		if (!carOptional.isPresent()) {
			log.info("Cartao de credito com numero: " + numero + " nao encontrado");
		}

		return ResponseEntity.ok(carOptional.get());
		
	}
    
    @GetMapping("/titular/{titular}")
   	public ResponseEntity<CartaoCredito> findByTitular(@PathVariable String titular) {
       	
       	Optional<CartaoCredito> carOptional = cartaoCreditoService.findByTitular(titular);
   		if (!carOptional.isPresent()) {
   			log.info("Cartao de credito com titularidade: " + titular + " nao encontrado");
   		}

   		return ResponseEntity.ok(carOptional.get());
   		
   	}
    
	@PostMapping
	public ResponseEntity<CartaoCredito> create(@Valid @RequestBody CartaoCredito cartaoCredito) {
		return ResponseEntity.ok(cartaoCreditoService.save(cartaoCredito));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<CartaoCredito> update(@PathVariable Long idCartaoAtual, @Valid @RequestBody CartaoCredito cartaoCreditoNovo) {
		
		try {
			return ResponseEntity.ok(cartaoCreditoService.update(idCartaoAtual, cartaoCreditoNovo));	
		}catch (Exception e) {
			log.error(String.format("nao foi possivel atualizar o cartao de credito: %d com dados %s devido erro: %s ",idCartaoAtual , cartaoCreditoNovo, e.getMessage()));
			return ResponseEntity.badRequest().build();
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable Long id) {
		try {
			cartaoCreditoService.deleteById(id);
			return ResponseEntity.ok().build();	
		}catch (Exception e) {
			log.error(String.format("nao foi possivel deletar o cartao de credito: %s devido erro:", id , e.getMessage()));
			return ResponseEntity.badRequest().build();
		}

	}

}
