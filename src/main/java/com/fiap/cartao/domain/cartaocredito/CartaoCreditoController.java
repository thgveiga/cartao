package com.fiap.cartao.domain.cartaocredito;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CartaoCreditoController {

	@GetMapping("/cartoes")
    public String list(){
        return "cartoes";
    }
	
}
