package com.fiap.cartao.domain.cartaocredito;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fiap.cartao.domain.aluno.Aluno;
import com.fiap.cartao.domain.aluno.AlunoService;
import com.fiap.cartao.domain.compra.Compra;
import com.fiap.cartao.domain.compra.CompraService;

@Service

public class CartaoCreditoService {

	@Autowired
	private CartaoCreditoRepository cartaoCreditoRespository;

	@Autowired
	private AlunoService alunoService;
	
	@Autowired
	private CompraService compraService;
	
	public List<CartaoCredito> findAll() {
		return cartaoCreditoRespository.findAll();
	}

	public Optional<CartaoCredito> findById(Long id) {
		return cartaoCreditoRespository.findById(id);
	}

	public Optional<CartaoCredito> findByNumero(Long numero) {
		return cartaoCreditoRespository.findByNumero(numero);
	}

	public Optional<CartaoCredito> findByTitular(String titular) {
		return cartaoCreditoRespository.findByTitular(titular);
	}
	
	private Aluno getAluno(String titular) {
		Optional<Aluno> optional = alunoService.findByNome(titular);
		if(!optional.isPresent())
			throw new IllegalArgumentException(String.format("O titular %s não existe ", titular));
		
		return optional.get();
		
	}
	
	public CartaoCredito save(CartaoCredito cartaoCredito) {
		Aluno aluno = getAluno(cartaoCredito.getTitular());
		aluno.setCartaoCredito(cartaoCredito);
		aluno.setUpdatedAt(LocalDateTime.now());
		return alunoService.update(aluno.getId(), aluno).getCartaoCredito();
	}

	public void deleteById(Long id) {
		
		Optional<CartaoCredito> optional = cartaoCreditoRespository.findById(id);
		
		if(!optional.isPresent())
			throw new IllegalArgumentException(String.format("O cartao de credito %d não existe ", id));
		
		CartaoCredito cartaoCredito = optional.get();
		
		Aluno aluno = getAluno(cartaoCredito.getTitular());
		aluno.setCartaoCredito(null);
		aluno.setUpdatedAt(LocalDateTime.now());
		
		alunoService.update(aluno.getId(), aluno);
		
		List<Compra> compras = cartaoCredito.getCompras();

		if(compras != null && !compras.isEmpty())
		  compraService.delete(compras);
		
		cartaoCreditoRespository.deleteById(id);
	}

	public CartaoCredito update(Long idCartaoAtual, @Valid CartaoCredito cartaoCreditoNovo) {

		Optional<CartaoCredito> optional = cartaoCreditoRespository.findById(idCartaoAtual);

		if (!optional.isPresent())
			throw new IllegalArgumentException(
					String.format("Cartao Credito com id %d não encntrado para efetuar atualizacao ", idCartaoAtual));

		CartaoCredito cartaoCreditoAtual = optional.get();
		
		cartaoCreditoAtual.setCompras(cartaoCreditoNovo.getCompras());
		cartaoCreditoAtual.setNumero(cartaoCreditoNovo.getNumero());		
		cartaoCreditoAtual.setTitular(cartaoCreditoNovo.getTitular());
		cartaoCreditoAtual.setLimite(cartaoCreditoNovo.getLimite());
		cartaoCreditoAtual.setUpdatedAt(LocalDateTime.now());
		
		return cartaoCreditoRespository.save(cartaoCreditoAtual);

	}

}
