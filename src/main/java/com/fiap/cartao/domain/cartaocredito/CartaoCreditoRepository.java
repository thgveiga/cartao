package com.fiap.cartao.domain.cartaocredito;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CartaoCreditoRepository extends JpaRepository<CartaoCredito, Long> {

	Optional<CartaoCredito> findByNumero(Long numero);

	Optional<CartaoCredito> findByTitular(String titular);
	
}
