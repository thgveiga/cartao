package com.fiap.cartao.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fiap.cartao.domain.aluno.Aluno;

@SpringBootTest
@AutoConfigureMockMvc
public class AlunoAPIUnitTests {
	
	@Test
	public void creatAluno(@Autowired MockMvc mvc) throws Exception {
		mvc.perform( MockMvcRequestBuilders
			      .post("/api/v1/alunos")
			      .content(asJsonString(new Aluno("RENAN FERREIRA MENDES", "3353150", "100-10")))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
		
		mvc.perform(get("/api/v1/alunos"))
    	.andExpect(status().isOk())
    	.andDo(MockMvcResultHandlers.print())
    	;
	}
	
	//@Test
    void getAllAlunosAPI(@Autowired MockMvc mvc) throws Exception {
        mvc.perform(get("/api/v1/alunos"))
        	.andExpect(status().isOk())
        	.andDo(MockMvcResultHandlers.print())
        	//.andExpect(content().string("[]"))
        	;
        
/*        mvc.perform( MockMvcRequestBuilders  
        	      .get("/api/v1/alunos")
        	      .accept(MediaType.APPLICATION_JSON))
        	      .andDo(MockMvcResultHandlers.print())
        	      .andExpect(status().isOk())
        	      //.andExpect(MockMvcResultMatchers.jsonPath("$.aluno").exists())
        	      //.andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
        	      ;
*/    }
    
	public static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}

}
