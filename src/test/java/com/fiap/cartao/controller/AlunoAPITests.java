package com.fiap.cartao.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.fiap.cartao.domain.aluno.Aluno;
import com.fiap.cartao.domain.aluno.AlunoAPI;
import com.fiap.cartao.domain.aluno.AlunoService;
import com.fiap.cartao.domain.cartaocredito.CartaoCredito;
import com.fiap.cartao.domain.cartaocredito.CartaoCreditoAPI;
import com.fiap.cartao.domain.cartaocredito.CartaoCreditoService;
import com.fiap.cartao.domain.compra.Compra;
import com.fiap.cartao.domain.compra.CompraAPI;
import com.fiap.cartao.domain.compra.CompraService;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AlunoAPITests {
	
	AlunoAPI alunoAPI = new AlunoAPI();
	CartaoCreditoAPI cartaoCreditoAPI = new CartaoCreditoAPI();
	CompraAPI compraAPI = new CompraAPI();

	@BeforeAll
	public void init(@Mock AlunoService alunoService, @Mock CartaoCreditoService cartaoCreditoService, @Mock CompraService compraService) {

		ReflectionTestUtils.setField(alunoAPI, "alunoService", criarAlunoServiceMock(alunoService));
		ReflectionTestUtils.setField(cartaoCreditoAPI, "cartaoCreditoService", createCartaoServiceMock(cartaoCreditoService));
		ReflectionTestUtils.setField(compraAPI, "compraService", compraService);
	}

	@BeforeAll
	void init(@Mock CompraService compraService) {
		Compra compra = new Compra();
		compra.setDescricao("Compra de teste");
		compra.setValor(100.0);;
		compra.setCreatedAt(LocalDateTime.now());
		compra.setNumeroCartao(5482999909878654L);
		
		List<Compra> compras = new ArrayList<>();
		compras.add(compra);
		
		when(compraService.save(any())).thenReturn(compra);
		when(compraService.findByNumeroCartao(anyLong())).thenReturn(Optional.of(compras));
	}

	private CartaoCreditoService createCartaoServiceMock(CartaoCreditoService cartaoCreditoService) {
		CartaoCredito cartaoCredito = new CartaoCredito();
		cartaoCredito.setNumero(5482999909878654L);
		cartaoCredito.setTitular("RENAN FERREIRA MENDES");
		cartaoCredito.setLimite(1000.0);
		
		List<CartaoCredito> cards = new ArrayList<>();
		cards.add(cartaoCredito);
		
		when(cartaoCreditoService.save(any())).thenReturn(cartaoCredito);
		when(cartaoCreditoService.findById(anyLong())).thenReturn(Optional.of(cartaoCredito));
		when(cartaoCreditoService.findAll()).thenReturn(cards);

		return cartaoCreditoService;
	}
	
	private AlunoService criarAlunoServiceMock(AlunoService alunoService) {

		List<Aluno> alunos = new ArrayList<>();
		
		Aluno a1 = new Aluno("RENAN FERREIRA MENDES", "3353150", "100-10");
		alunos.add(a1);
		
		Aluno a2 = new Aluno("MARIO JORGE", "5533440", "100-11");
		alunos.add(a2);
		
		when(alunoService.save(alunos)).thenReturn(alunos);
		
		when(alunoService.findById(any())).thenReturn(Optional.of(a1));
		when(alunoService.findById(any())).thenReturn(Optional.of(a2));
		
		when(alunoService.findAll()).thenReturn(alunos);

		return alunoService;
	}
	
	@Test
	public void getAllAlunos() {
		assertEquals(2, alunoAPI.findAll().getBody().size());
	}


	@Test
	public void createCartao() {
		assertEquals(5482999909878654L, cartaoCreditoAPI.create(new CartaoCredito()).getBody().getNumero());
	}

	@Test
	public void createCompra() {
		assertEquals(100.0, compraAPI.create(new Compra()).getBody().getValor());
		assertEquals("Compra de teste", compraAPI.create(new Compra()).getBody().getDescricao());
	}

	@Test
	public void listByCard() {
		assertEquals(1, compraAPI.findByNumeroCartao(5482999909878654L).getBody().size());
	}
	
	
	@Test
	public void criarAluno() {
		assertEquals("1122330", alunoAPI.create(new Aluno("JOSE", "1122330", "100-12")).getBody().getRa());
	}
	
	
}

